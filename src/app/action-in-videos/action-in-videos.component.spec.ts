import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionInVideosComponent } from './action-in-videos.component';

describe('ActionInVideosComponent', () => {
  let component: ActionInVideosComponent;
  let fixture: ComponentFixture<ActionInVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionInVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionInVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
