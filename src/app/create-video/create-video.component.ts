import { Component, OnInit } from '@angular/core';
import {VideoStudioService} from '../services/video-studio.service';
import {VideoTemplate} from '../models/VideoTemplate';
import {StatService} from '../services/stat.service';
import {VideoExtraAction} from '../models/VideoExtraAction';
import {messages} from '../common/messages';
import {Common} from '../common/common';

@Component({
  selector: 'app-create-video',
  templateUrl: './create-video.component.html',
  styleUrls: ['./create-video.component.css']
})
export class CreateVideoComponent implements OnInit {

  constructor(
    private service: VideoStudioService,
    private statService: StatService
  ) { }

  ngOnInit() {
    this.initTemplateSelected();
  }

  private initTemplateSelected() {
    this.service.templateSelected = this.service.listVideoTemplates[0];
    const s: any = document.getElementById('videoTemplateSelectMenu');
    s.selectedIndex = 0;
  }

  getRemainingVideoExtraActionSlots() {
    const maxSlots = this.statService.getMaxVideoExtraActionSlots();
    return this.service.getRemainingVideoExtraActionSlots(maxSlots);
  }

  addActionToVideo(extraAction: VideoExtraAction) {
    // control the remaining slots in the component before sending the action to the service
    if (this.getRemainingVideoExtraActionSlots() <= 0) {
      Common.notify(messages.NOT_ENOUGH_SLOTS);
      return;
    } else if (this.statService.getCurrentLevel() ) {
      // control the level needed by the action
    } else {
      this.service.addActionToVideo(extraAction);
    }
  }

  publishVideo() {
    // check enough internet bills to publish
    if (! this.statService.checkInternetBillsToPublishVideo()) {
      Common.notify(messages.NOT_ENOUGH_INTERNET_BILLS_PUBLISH);
      return;
    }

    // check enough money, to afford the video
    const currentEarnedMoney = this.service.getCurrentEarnedMoney();
    if (currentEarnedMoney < 0 && this.statService.money + currentEarnedMoney <= 0) {
      Common.notify(messages.NOT_ENOUGH_MONEY_PUBLISH);
      return;
    }

    this.statService.publishVideo(
      this.service.getCurrentEarnedLikes(),
      this.service.getCurrentEarnedMoney(),
      this.service.getEarnedSubscribers()
    );

    this.service.clearLists();
    this.initTemplateSelected();
  }

  selectedTemplate(template: string) {
    let i = 0;
    for (const videoTemplate of this.service.listVideoTemplates) {
      if (videoTemplate.name === template) {
        // dirty condition : check if template ready
        // it not, check also that its not the first tamper template (
        if (videoTemplate.ready === false && i !== 0) {
          Common.notify(messages.TEMPLATE_COOLDOWN);
          return;
        }
        if (this.statService.isLevelOf(videoTemplate.levelNeeded) === true) {
          this.service.templateSelected = videoTemplate;
        } else {
          Common.notify(messages.NOT_ENOUGH_LEVEL);
          this.initTemplateSelected();
        }
        return;
      }
      i++;
    }
  }

}
