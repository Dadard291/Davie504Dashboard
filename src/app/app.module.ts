import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ActionInVideosComponent } from './action-in-videos/action-in-videos.component';
import { CreateVideoComponent } from './create-video/create-video.component';
import { StatComponent } from './stat/stat.component';

@NgModule({
  declarations: [
    AppComponent,
    ActionInVideosComponent,
    CreateVideoComponent,
    StatComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
