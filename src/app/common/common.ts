
export class Common {
  public static lose(msg: string) {
    alert(msg);
    window.location.reload();
  }

  public static win(msg: string) {
    alert(msg);
    window.location.reload();
  }

  public static notify(msg: string): void {
    alert(msg);
  }
}
