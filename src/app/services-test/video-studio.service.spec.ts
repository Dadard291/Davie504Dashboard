import { TestBed } from '@angular/core/testing';

import { VideoStudioService } from '../services/video-studio.service';

describe('VideoStudioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoStudioService = TestBed.get(VideoStudioService);
    expect(service).toBeTruthy();
  });
});
