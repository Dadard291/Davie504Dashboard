import { Injectable } from '@angular/core';
import {config} from '../../assets/config';
import {VideoExtraAction} from '../models/VideoExtraAction';
import {VideoTemplate} from '../models/VideoTemplate';

@Injectable({
  providedIn: 'root'
})
export class VideoStudioService {
  public readonly listVideoTemplates: VideoTemplate[] = config.videoCreation.videoTemplates;

  // specials templates
  public readonly listCheckMatesCategories = config.videoCreation.checkMateCategories;
  public readonly  listBossBassBattle = config.videoCreation.bossBassBattle;

  public readonly  listVideoExtraActions: VideoExtraAction[] = config.videoCreation.videoExtraActions;

  public readonly currentActionsInVideo: VideoExtraAction[];
  public templateSelected: VideoTemplate;

  public currentEarnedLikes = 0;
  public currentEarnedMoney = 0;
  public currentEarnedSubscribers = 0;


  constructor() {
    this.currentActionsInVideo = [];
  }

  getCurrentEarnedLikes() {
    return this.currentEarnedLikes + this.templateSelected.baseLikes;
  }

  getCurrentEarnedMoney() {
    return this.currentEarnedMoney - this.templateSelected.moneyCost;
  }

  getEarnedSubscribers() {
    return this.currentEarnedSubscribers;
  }

  addActionToVideo(extraAction: VideoExtraAction) {
    this.currentEarnedLikes += extraAction.likes;
    this.currentEarnedMoney += extraAction.money;
    this.currentEarnedSubscribers += extraAction.subscribers;
    this.currentActionsInVideo.push(extraAction);
    return extraAction;
  }

  removeActionFromVideo(extraAction: VideoExtraAction) {
    let i = 0;
    for (const e of this.currentActionsInVideo) {
      if (e.name === extraAction.name) {
        this.currentActionsInVideo.splice(i, 1);
        this.currentEarnedLikes -= extraAction.likes;
        this.currentEarnedMoney -= extraAction.money;
        this.currentEarnedSubscribers -= extraAction.subscribers;
        return e;
      }
      i += 1;
    }
  }

  getRemainingVideoExtraActionSlots(maxSlots: number) {
    return maxSlots - this.currentActionsInVideo.length;
  }

  clearLists() {
    this.currentEarnedLikes = 0;
    this.currentEarnedMoney = 0;
    this.currentEarnedSubscribers = 0;
    this.currentActionsInVideo.splice(0, this.currentActionsInVideo.length);

    // set cooldown before the template is ready again
    // let i = 0;
    // for (const vt of this.listVideoTemplates) {
    //   if (vt.name === this.templateSelected.name) {
    //     break;
    //   }
    //   i++;
    // }

    const objectTemplateSelected = this.templateSelected;
    objectTemplateSelected.ready = false;

    const o: any = document.getElementById(objectTemplateSelected.name + 'Id');
    o.disabled = true;


    const delay = objectTemplateSelected.cooldown;
    const setCooldown = setTimeout(function() {
      o.disabled = false;
      objectTemplateSelected.ready = true;
    },
      delay * 1000
    );
  }
}
