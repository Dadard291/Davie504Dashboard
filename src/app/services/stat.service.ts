import { Injectable } from '@angular/core';
import {config} from '../../assets/config';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  waterBills = config.stats.waterBillsMax;
  internetBills = config.stats.internetBillsMax;

  // will increase according to the levels
  waterBillsMax = config.stats.waterBillsMax;
  internetBillsMax = config.stats.internetBillsMax;

  public likes = config.stats.likesBase;
  public subscribers = config.stats.subscribersBase;
  public money = config.stats.moneyBase;

  constructor() { }

  getCurrentLevel() {
    const subNumber = this.subscribers;
    if (subNumber < config.stats.levels.LEVEL_1) {
      return 0;
    } else if (subNumber < config.stats.levels.LEVEL_2) {
      return 1;
    } else if (subNumber < config.stats.levels.LEVEL_3) {
      return 2;
    } else if (subNumber < config.stats.levels.LEVEL_4) {
      return 3;
    } else if (subNumber < config.stats.levels.LEVEL_5) {
      return 4;
    } else {
      return 5;
    }
  }

  getMaxVideoExtraActionSlots() {
    const base = config.videoCreation.baseSlotsNumber;
    return base + this.getCurrentLevel();
  }

  isLevelOf(level: number) {
    return this.getCurrentLevel() >= level;
  }

  getMoneyToPayForInternetBills() {
    return config.stats.internetBillsMax - this.internetBills;
  }

  getMoneyToPayForWaterBills() {
    return config.stats.waterBillsMax - this.waterBills;
  }

  getSubscribersPercentage() {
    return Math.round(this.subscribers *  100 / config.stats.subscribersMax);
  }

  checkWin(): boolean {
    return this.subscribers >= config.stats.subscribersMax;
  }

  publishVideo(earnedLikes, earnedMoney, earnedSubscribers) {
    this.internetBills -= this.getInternetBillsCostToPublishVideo();
    this.likes += earnedLikes;
    this.money += earnedMoney;
    this.subscribers += earnedSubscribers;
  }

  getInternetBillsCostToPublishVideo() {
    return Math.round(this.internetBillsMax * 30 / 100);
  }

  checkInternetBillsToPublishVideo(): boolean {
    return this.internetBills > this.getInternetBillsCostToPublishVideo();
  }

}
