export class VideoExtraAction {
  name: string;
  likes: number;
  subscribers: number;
  money: number;
  levelNeeded: number;
}
