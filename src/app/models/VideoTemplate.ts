export class VideoTemplate {
  name: string;
  baseLikes: number;
  moneyCost: number;
  levelNeeded: number;
  cooldown: number;
  ready: boolean;
}
