import { Component, OnInit } from '@angular/core';
import {StatService} from '../services/stat.service';
import {config} from '../../assets/config';
import {Common} from '../common/common';
import {messages} from '../common/messages';

@Component({
  selector: 'app-stat',
  templateUrl: './stat.component.html',
  styleUrls: ['./stat.component.css']
})
export class StatComponent implements OnInit {

  constructor(
    private service: StatService
  ) { }

  ngOnInit() {
    const service: StatService = this.service;

    // decrease continuously the water bills, so davie can survive
    const delayWater = 1.5;
    const decreaseWaterBills = setInterval(function() {
        if (service.waterBills > config.stats.waterBillsMin) {
          service.waterBills -= 1;
        } else {
          Common.lose(messages.LOST_WATER_BILLS);
        }
      },
      delayWater * 1000);

    // decrease continuously the internet bills, cause davies's continuously watching bass videos
    const delayInternet = 1;
    const decreaseInternetBills = setInterval(function() {
      if (service.internetBills > config.stats.internetBillsMin) {
        service.internetBills -= 1;
      } else {
        Common.lose(messages.LOST_INTERNET_BILLS);
      }
    },
      delayInternet * 1000);

    // increase the money according to the likes
    const delayMoney = 0.2;
    const increaseMoney = setInterval(function() {
      const likeProportion = 1 / 100000;
      service.money = Number((service.money + service.likes * likeProportion).toFixed(2));
    },
      delayMoney * 1000);

    // increase the subs according to the likes
    const delaySubs = 0.15;
    const increaseSubs = setInterval(function() {
      if (service.checkWin()) {
        Common.win(messages.WIN);
      }
      const likeProportion = 1 / 100;
      service.subscribers = Number((service.subscribers + service.likes * likeProportion).toFixed(2));
    },
      delaySubs * 1000);

  }

  payForWaterBills() {
    const moneyToPay = this.service.getMoneyToPayForWaterBills();
    if (moneyToPay > this.service.money) {
      this.service.waterBills += Math.round(this.service.money);
      this.service.money = 0;
    } else {
      this.service.money -= moneyToPay;
      this.service.waterBills = config.stats.waterBillsMax;
    }
  }

  payForInternetBills() {
    const moneyToPay = this.service.getMoneyToPayForInternetBills();
    if (moneyToPay > this.service.money) {
      this.service.internetBills += Math.round(this.service.money);
      this.service.money = 0;
    } else {
      this.service.money -= moneyToPay;
      this.service.internetBills = config.stats.internetBillsMax;
    }
  }

  printSubscribersNumber() {
    return (this.service.subscribers / 1000).toFixed(0) + 'k';
  }

  printLikesNumber() {
    return (this.service.likes / 1000).toFixed(1) + 'k';
  }

}
