# Davie504Dashboard

## RULES

### Create a video

You can create a video with the following templates :

| name                | base-likes | money-cost | subscribes-needed | cooldown |
|---------------------|------------|------------|-------------------|----------|
| bass battle         | 700        | 0          | 4 millions        | 3 min    |
| fiverr              | 300        | 300        | 0                 | 1 min    |
| reddit review       | 100        | 0          | 1 million         | 1 min    |
| travel              | 500        | 500        | 2 millions        | 2 min    |
| video games project | 300        | 0          | 0                 | 5 min    |
| SDAIAY              | 100        | 0          | 0                 | 1 min    |
| cook italian        | 400        | 50         | 3 millions        | 5 min    |

The base-likes are the earned likes given by the template. You can earn more **likes** or earn **subscribers** by adding extra actions in the videos.

| name                          | likes | subscribers | money | subscribers-needed |
|-------------------------------|-------|-------------|-------|--------------------|
| Slap like now                 | 10    | 0           | 0     | 0                  |
| Leave a comment now           | 5     | 5           | 0     | 0                  |
| Therapy time                  | 0     | 5           | 50    | 0                  |
| *Threaten for more subscribe* | -5    | 15          | 0     | 2 millions         |
| *Threaten for more likes*     | 15    | -5          | 0     | 1 millions         |
| OMG                           | 5     | 0           | 0     | 0                  |
| WHAT                          | 5     | 0           | 0     | 0                  |
| unbelievable                  | 0     | 5           | 0     | 0                  |
| pff                           | 0     | 5           | 0     | 0                  |
| epico                         | 10    | 15          | 0     | 3 millions         |
| *speak italian*               | 30    | 20          | 0     | 4 millions         |
| *wink*                        | 10    | 5           | 0     | 0                  |
| checkmate                     | depends on the target                            |

The number of extra actions for 1 video increases with the subscribers number.



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Sources
Icons by <a target="_blank" href="https://icons8.com">Icons8</a>
